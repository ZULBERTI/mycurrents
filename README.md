# MyCurrents and Acoustic Doppler Current Profilers IO tools

Oceanographic instrumentation processing tools

Currently includes tools for processing:
    
 - Seabird *.hex and *cnv
 - RDI ADCP (*.000)

Much of the code has been "borrowed" from two sources:
     
 - [UHDAS Currents toolbox](http://currents.soest.hawaii.edu/docs/adcp_doc/codas_doc/index.html)
 - ["dolfyn" package on github](https://github.com/lkilcher/dolfyn)

---
## Requirements

 -[Dolfyn package](https://github.com/lkilcher/dolfyn)


---
## Versions

 - v0.1.0 : Python 2.7 compatible version


---
Matt Rayson

University of Western Australia

April 2016
